<?php

$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise_backend';

$conn = new mysqli($host, $username, $password, $database);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Function to execute SQL queries
function executeQuery($conn, $sql, $message)
{
    if ($conn->query($sql) === TRUE) {
        echo $message . " Successful<br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error . "<br>";
    }
}

// Handle form submissions
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Create
    if (isset($_POST['create'])) {
        // Validate and sanitize input before using in SQL query
        $first_name = mysqli_real_escape_string($conn, $_POST['first_name']);
        $last_name = mysqli_real_escape_string($conn, $_POST['last_name']);
        $birthday = mysqli_real_escape_string($conn, $_POST['birthday']);
        $address = mysqli_real_escape_string($conn, $_POST['address']);

        $sqlCreate = "INSERT INTO employee (first_name, last_name, birthday, address) 
                      VALUES ('$first_name', '$last_name', '$birthday', '$address')";
        executeQuery($conn, $sqlCreate, "Insert");
    }

    // Read (Retrieve All)
    if (isset($_POST['retrieveAll'])) {
        $sqlSelectAll = "SELECT * FROM employee";
        $resultSelectAll = $conn->query($sqlSelectAll);

        if ($resultSelectAll->num_rows > 0) {
            echo "<h2>All Employee</h2>";
            while ($row = $resultSelectAll->fetch_assoc()) {
                echo "ID: " . htmlspecialchars($row["id"]) . " - Name: " . htmlspecialchars($row["first_name"]) . " " . htmlspecialchars($row["last_name"]) . " - Birthday: " . htmlspecialchars($row["birthday"]) . " - Address: " . htmlspecialchars($row["address"]) . "<br>";
            }
        } else {
            echo "0 results<br>";
        }
    }

    // Update
    if (isset($_POST['update'])) {
        // Validate and sanitize input before using in SQL query
        $idToUpdate = mysqli_real_escape_string($conn, $_POST['idToUpdate']);
        $newAddress = mysqli_real_escape_string($conn, $_POST['newAddress']);

        $sqlUpdate = "UPDATE employee SET address = '$newAddress' WHERE id = $idToUpdate";
        executeQuery($conn, $sqlUpdate, "Update");
    }

    // Delete
    if (isset($_POST['delete'])) {
        // Validate and sanitize input before using in SQL query
        $idToDelete = mysqli_real_escape_string($conn, $_POST['idToDelete']);

        $sqlDelete = "DELETE FROM employee WHERE id = $idToDelete";
        executeQuery($conn, $sqlDelete, "Delete");
    }
}

$conn->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee CRUD</title>
</head>
<body>
    <h1>Task 7</h1>

    <!-- Create Form -->
    <form method="POST">
        <h2>Create Employee</h2>
        <label for="first_name">First Name:</label>
        <input type="text" id="first_name" name="first_name" required>
        <label for="last_name">Last Name:</label>
        <input type="text" id="last_name" name="last_name" required>
        <label for="birthday">Birthday:</label>
        <input type="date" id="birthday" name="birthday" required>
        <label for="address">Address:</label>
        <input type="text" id="address" name="address" required>
        <button type="submit" name="create">Create Employee</button>
    </form>

    <!-- Retrieve All Employees -->
    <form method="POST" name="retrieveAllForm">
        <h2>Retrieve All Employees</h2>
        <button type="submit" name="retrieveAll">Retrieve All</button>
    </form>

    <!-- Update Form -->
    <form method="POST">
        <h2>Update Employee Address</h2>
        <label for="idToUpdate">Employee ID:</label>
        <input type="number" id="idToUpdate" name="idToUpdate" required>
        <label for="newAddress">New Address:</label>
        <input type="text" id="newAddress" name="newAddress" required>
        <button type="submit" name="update">Update Address</button>
    </form>

    <!-- Delete Form -->
    <form method="POST">
        <h2>Delete Employee</h2>
        <label for="idToDelete">Employee ID:</label>
        <input type="number" id="idToDelete" name="idToDelete" required>
        <button type="submit" name="delete">Delete Employee</button>
    </form>
</body>
</html>
