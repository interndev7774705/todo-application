<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function _before(ApiTester $I)
    {
    }


     // Scenario 2: Creating New Tasks
     public function iShouldCreateNewTasks(ApiTester $I)
     {
         // Send a POST request to the create task endpoint with task details
         $I->haveHttpHeader('content-type', 'application/json');
         $I->sendPost('/API.php', [
            'id' => 1,
            'task_title' => 'task9',
            'task_name' => 'programming',
            'time'=> "2024-03-21T12:00:00",
            'status' => 'Inprogress'
         ]);
         
         // Validate the response
         $I->seeResponseCodeIs(200);
         $I->seeResponseIsJson();
         $I->seeResponseContainsJson(['status' => 'success']);
     }
 
     
// Scenario 3: Views the list of tasks
public function iShouldViewListOfTasks(ApiTester $I)
{
    // Set the HTTP header
    $I->haveHttpHeader('Content-Type', 'application/json');
    
    // Send a GET request to the endpoint to fetch the list of tasks
    $I->sendGet('/Api.php');
    
    // Validate the response
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContainsJson(['status' => 'success', 'message' => 'Success']);
}
     
     // Scenario 4: Update Task to Done section
     public function iShouldUpdateTaskToDoneSection(ApiTester $I)
     {
        
        $I->haveHttpHeader('Content-Type', 'application/json');
        $requestData = [
            'id' => 1, 
            'status' => 'Done', 
        ];
        $I->sendPut('/Api.php/1', json_encode($requestData)); // Sending PUT request to update task status
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success', 'message' => 'Task updated successfully']); // Checking what the response says

     }
     
    // Scenario 5: Task to Back to the inprogress section (Test function to move a task back to "In Progress")
    public function iShouldMoveTaskBackToInProgress(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $requestData = [
            'id' => 3, // Specify the task ID
            'status' => 'Inprogress', // Move status back to In Progress
        ];
        $I->sendPut('/Api.php/1', json_encode($requestData)); // Sending PUT request to update task status
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success', 'message' => 'Task updated successfully']); // Checking what the response says
    }

     
    // Scenario 6: Editing Task Details (Test function to update an existing task)
    public function iShouldUpdateExistingTask(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $requestData = [
            'id' => 4, 
            'task_title' => 'Sample Task',
            'task_name' => 'Sample Task',
        ];
        $I->sendPut('/Api.php/1', json_encode($requestData)); // Sending PUT request to update task
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); 
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([ // Checking what the response says
            'status' => 'success',
            'message' => 'Task updated successfully',
        ]); 
    }

     
     // Scenario 7: Deleting Task Details
     public function iShouldDeleteTaskDetails(ApiTester $I)
     {
         // Send a DELETE request to delete a task
         $I->sendDelete('/Api.php/1');
         
         // Validate the response
         $I->seeResponseCodeIs(200);
         $I->seeResponseIsJson();
         $I->seeResponseContainsJson(['status' => 'success']);
     }
}
