<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class FailTestTodoListCest
{
    public function _before(ApiTester $I)
    {
        // This method is executed before each test method
    }
    
    // Scenario: Failing to insert data due to missing fields
    public function iShouldFailToInsertDataDueToMissingFields(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $payload = ['task_title' => 'Incomplete Task'];
        $I->sendPost('/Api.php', json_encode($payload));
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'error', 'message' => 'Incomplete payload for task creation']);
    }

    // Scenario: Failing to retrieve specific task due to invalid id
    public function iShouldFailToRetrieveSpecificTaskDueToInvalidId(ApiTester $I)
    {
        $I->sendGet('/Api.php?id=nonexistent');
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'error', 'message' => 'Failed Fetch Request']);
    }

    // Scenario: Failing to update task to done for nonexistent task
    public function iShouldFailToUpdateTaskToDoneForNonexistentTask(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $requestData = [
            'id' => 99, // Nonexistent Task ID
            'status' => 'Done', // Attempt to update status to Done
        ];
        $I->sendPut('/Api.php', json_encode($requestData));
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'error', 'message' => 'Failed to update task']);
    }

    // Scenario: Failing to move task back to in progress for nonexistent task
    public function iShouldFailToMoveTaskBackToInProgressForNonexistentTask(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $requestData = [
            'id' => 99, // Nonexistent Task ID
            'status' => 'Inprogress', // Attempt to move status back to In Progress
        ];
        $I->sendPut('/Api.php', json_encode($requestData));
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'error', 'message' => 'Failed to update task']);
    }
    
    // Scenario: Failing to update nonexistent task
    public function iShouldFailToUpdateNonexistentTask(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/Api.php?id=999', ['task_title' => 'Nonexistent Task Title', 'task_name' => 'Trying to update a task that does not exist']);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'error', 'message' => 'Failed to update task']);
    }

// Scenario: Failing to delete nonexistent task
public function iShouldFailToDeleteNonexistentTask(ApiTester $I)
{
    $I->haveHttpHeader('Content-Type', 'application/json');
    
    // Assuming ID 999 does not exist in the database
    $nonExistentId = 999;
    
    // Check if the task exists before attempting to delete it
    $taskExists = $this->checkTaskExists($nonExistentId);
    
    if ($taskExists) {
        // If the task exists, fail the test as it should not exist
        $I->fail("Task with ID $nonExistentId should not exist but was found.");
    } else {
        // If the task does not exist, attempt to delete it
        $payload = ['id' => $nonExistentId];
        $I->sendDelete('/Api.php', $payload);
        
        // Expect a response indicating failure to delete the task
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'error', 'message' => 'Failed to delete task']);
    }
}

// Function to check if a task exists in the database
private function checkTaskExists($taskId)
{
    // Implement your logic to check if the task exists in the database
    // Return true if the task exists, false otherwise
}

}
