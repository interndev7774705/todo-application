<?php
require_once('MysqliDb.php');

class API {
    private $db;

    public function __construct()
    {
        // Database connection settings
        $db_host = 'localhost'; // Database host
        $db_user = 'root';      // Database username
        $db_pass = '';          // Database password
        $db_name = 'employee';  // Database name

        // Create a new database connection
        $this->db = new mysqliDb($db_host, $db_user, $db_pass, $db_name);
    }

    /**
     *  HTTP GET Request
     * 
     * @param array $payload
 */
public function httpGet($payload)
{
    $id = $_GET['id'] ?? null;

    if (!$id) {
        // No ID provided, fetch all records
        $results = $this->db->get('tbl_to_do_list', null, ['task_title', 'task_name', 'time']);
        if ($results) {
            $this->successResponse("Success", $results);
        } else {
            $this->errorResponse("No tasks found", 'GET');
        }
    } else {
        // ID provided, fetch specific record
        $result = $this->db->where('id',  $id)->getOne('tbl_to_do_list', ['task_title', 'task_name', 'time']);
        if ($result) {
            $this->successResponse("Success", $result);
        } else {
            $this->errorResponse("Failed Fetch Request", 'GET');
        }
    }
}

    /**
     * HTTP POST Request
     *
     * @param array $payload
     */
    public function httpPost($payload)
    {
        if(!is_array($payload) || empty($payload)){
            $this->ErrorResponse("Invalid payload format", 'POST');
        }
        
        if(isset($payload['task_title']) && isset($payload['task_name']) && isset($payload['time'])){

            if(isset($payload['id'])){
                $existingRecord = $this->db->where('id', $payload['id'])->getOne('tbl_to_do_list');
                if($existingRecord){
                    $this->ErrorResponse("ID already exists", 'POST');
                }
            }

            $insertData = [
                'id' => $payload['id'],
                'task_title' => $payload['task_title'],
                'task_name' => $payload['task_name'],
                'time' => $payload['time'],
                'status' => 'Inprogress'
            ];

            $id = $this->db->insert('tbl_to_do_list', $insertData);

            if($id){
                $this->SuccessResponse('Data inserted successfully', $payload);
            }else{
                $this->ErrorResponse("Failed to Insert Data", 'POST');
            }
        }else {
            $this->ErrorResponse("Incomplete payload for task creation", 'POST');
        }
    }

  /**
 * HTTP PUT Request
 *
 * @param int $id
 * @param array $payload
 */
public function httpPut($id, $payload)
{
    // Check if ID is provided
    if (!$id) {
        $this->errorResponse("ID not provided in the request", 'PUT');
    }

    // Check if payload is empty or not an array
    if (!is_array($payload) || empty($payload)) {
        $this->errorResponse("Invalid payload format: " . json_encode($payload), 'PUT');
    }

    // Check if the task exists in the database
    $existingTask = $this->db->where('id', $id)->getOne('tbl_to_do_list');

    if (!$existingTask) {
        $this->errorResponse("Failed to update task", 'PUT');
    }

    // Update task details
    $data = [
        'task_title' => $payload['task_title'] ?? $existingTask['task_title'],
        'task_name' => $payload['task_name'] ?? $existingTask['task_name'],
        'time' => $payload['time'] ?? $existingTask['time'],
        // Add any other fields you want to update
    ];

    $result = $this->db->where('id', $id)->update('tbl_to_do_list', $data);

    if ($result) {
        $this->successResponse("Task updated successfully", $payload);
    } else {
        $this->errorResponse("Failed to update task", 'PUT');
    }
}




    /**
 * HTTP DELETE Request
 *
 * @param int $id
 * @param array $payload
 */
public function httpDelete($id, $payload)
{
    // Check if ID is provided
    if (!$id) {
        $this->errorResponse("ID not provided in the request", 'DELETE');
    }

    // Check if the task exists before attempting to delete it
    $existingRecord = $this->db->where('id', $id)->getOne('tbl_to_do_list');

    if ($existingRecord) {
        // Delete task
        $result = $this->db->where('id', $id)->delete('tbl_to_do_list');

        if ($result) {
            $this->successResponse("Task deleted successfully", $payload);
        } else {
            $this->errorResponse("Failed to delete task", 'DELETE');
        }
    } else {
        // If the task does not exist, return an error response
        $this->errorResponse("Failed to delete task", 'DELETE');
    }
}


    // Other methods of the API class...

    // Function to send success response
    private function successResponse($message, $data)
    {
        $response = array(
            "status" => "success",
            "message" => $message,
            "data" => $data
        );
        header('Content-Type: application/json');
        echo json_encode($response);
        exit; // Terminate script execution after sending success response
    }

    // Function to send error response
    private function errorResponse($message)
    {
        $response = array(
            "status" => "error",
            "message" => $message
        );
        header('Content-Type: application/json');
        echo json_encode($response);
        exit; // Terminate script execution after sending error response
    }
}

// Create a new instance of API class
$api = new API;

// Define a variable to hold the received data
$received_data = null;

// Define a variable to identify the type of request
$request_method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '';

// For GET, POST, PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    // Check if method is PUT or DELETE, and get the ids from URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));
        $last_index = count($exploded_request_uri) - 1;
        $ids = $exploded_request_uri[$last_index];
    }

    // Decode payload data for POST request
  
        $received_data = json_decode(file_get_contents('php://input'), true);
    
}

// Use a switch statement to call the appropriate function from the $api instance
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
    default:
        // If the request method is unsupported, send an error response
     //   $api->errorResponse("Unsupported HTTP method");
}
?>
