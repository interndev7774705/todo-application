<?php
$host = 'localhost';
$username = 'root';
$password = '';
$database = 'p8_exercise-backend';

$conn = mysqli_connect($host, $username, $password, $database);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Function to execute SQL queries
function executeQuery($sql, $message)
{
    global $conn;
    if ($conn->query($sql) === TRUE) {
        echo $message . " Successful<br>";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error . "<br>";
    }
}

// Insert data into the employee table
if (isset($_POST['insert'])) {
    $sqlInsert = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) 
                  VALUES ('Jane', 'Doe', 'Marie', '1990-01-01', '456 Elm Street')";
    executeQuery($sqlInsert, "Insert");
}

// Retrieve first name, last name, and birthday of all employees
if (isset($_POST['retrieveAll'])) {
    $sqlSelectAll = "SELECT first_name, last_name, birthday FROM employee";
    $resultSelectAll = $conn->query($sqlSelectAll);

    if ($resultSelectAll->num_rows > 0) {
        while ($row = $resultSelectAll->fetch_assoc()) {
            echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Birthday: " . $row["birthday"] . "<br>";
        }
    } else {
        echo "0 results<br>";
    }
}

// Retrieve the number of employees whose last name starts with the letter 'D'
if (isset($_POST['countD'])) {
    $sqlCountD = "SELECT COUNT(*) as countD FROM employee WHERE last_name LIKE 'D%'";
    $resultCountD = $conn->query($sqlCountD);

    if ($resultCountD->num_rows > 0) {
        while ($row = $resultCountD->fetch_assoc()) {
            echo "Number of Employees with Last Name starting with 'D': " . $row["countD"] . "<br>";
        }
    } else {
        echo "0 results<br>";
    }
}

// Retrieve first name, last name, and address of the employee with the highest ID number
if (isset($_POST['maxID'])) {
    $sqlMaxID = "SELECT first_name, last_name, address FROM employee ORDER BY id DESC LIMIT 1";
    $resultMaxID = $conn->query($sqlMaxID);

    if ($resultMaxID->num_rows > 0) {
        while ($row = $resultMaxID->fetch_assoc()) {
            echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Address: " . $row["address"] . "<br>";
        }
    } else {
        echo "0 results<br>";
    }
}

// Update data in the employee table
if (isset($_POST['updateJohn'])) {
    $sqlUpdateJohn = "UPDATE employee SET address = '123 Main Street' WHERE first_name = 'John'";
    executeQuery($sqlUpdateJohn, "Update");
}

// Delete all employees whose last name starts with the letter 'D'
if (isset($_POST['deleteD'])) {
    $sqlDeleteD = "DELETE FROM employee WHERE last_name LIKE 'D%'";
    executeQuery($sqlDeleteD, "Delete");
}

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee Management</title>
</head>
<body>
    <h1>Employee Management</h1>

    <form method="POST">
        <button type="submit" name="insert">Insert Employee</button>
        <button type="submit" name="retrieveAll">Retrieve All Employees</button>
        <button type="submit" name="countD">Count Employees with Last Name 'D'</button>
        <button type="submit" name="maxID">Employee with Highest ID</button>
        <button type="submit" name="updateJohn">Update John's Address</button>
        <button type="submit" name="deleteD">Delete Employees with Last Name 'D'</button>
    </form>
</body>
</html>
