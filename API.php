<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');

class API {
    public function __construct()
    {
        $this->db = new MysqliDb('localhost', 'root', '', 'ojt_juanhr');
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
        // Check if id is not null or empty
        if (empty($id)) {
            $response = [
                'status' => 'error',
                'message' => 'ID cannot be null or empty.',
            ];
            $this->sendResponse($response);
        }

        // Check if $payload is not empty
        if (empty($payload)) {
            $response = [
                'status' => 'error',
                'message' => 'Payload cannot be empty.',
            ];
            $this->sendResponse($response);
        }

        // Check if the id passed in matches the id in the payload
        if ($id != $payload['id']) {
            $response = [
                'status' => 'error',
                'message' => 'ID mismatch between parameter and payload.',
            ];
            $this->sendResponse($response);
        }

        // Check if payload is an array or not
        if (!is_array($payload)) {
            // Not an array, use the where() function from the MysqliDB class to specify the ID
            $this->db->where('id', $id);
        } else {
            // Array, use the SQL IN operator in the where() function to specify the ID
            $this->db->where('id', $payload, 'IN');
        }

        // Execute query to delete data from the database
        $result = $this->db->delete('your_table_name'); // Replace 'your_table_name' with your actual table name

        // Check if the query was successful
        if ($result) {
            // Successful query
            $response = [
                'status' => 'success',
                'message' => 'Data deleted successfully.',
            ];
        } else {
            // Failed query
            $response = [
                'status' => 'fail',
                'message' => 'Failed to Delete Data',
            ];
        }

        // Send the response
        $this->sendResponse($response);
    }

    /**
     * Send JSON response
     *
     * @param array $response
     */
    private function sendResponse($response)
    {
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }
}

?>

