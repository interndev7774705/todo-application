<?php

class API {
    public function printFullName($fullName) {
        echo "Full Name: $fullName\n";
    }

    public function printHobbies($hobbies) {
        echo "Hobbies:\n";
        foreach ($hobbies as $hobby) {
            echo "    $hobby\n";
        }
    }

    public function printPersonalInfo($personalInfo) {
        echo "Age: {$personalInfo->age}\n";
        echo "Email: {$personalInfo->email}\n";
        echo "Birthday: {$personalInfo->birthday}\n";
    }
}

$api = new API();

$api->printFullName("Charles Joseph M. Rioveros");
echo "\n";

$hobbies = ["Playing Basketball", "Playing Online Games", "Listening Music"];
$api->printHobbies($hobbies);
echo "\n";

$personalInfo = (object) [
    'age' => 22,
    'email' => 'charles05rioveros@gmail.com',
    'birthday' => 'December 09,2001'
];
$api->printPersonalInfo($personalInfo);
echo "\n";

?>
